import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from '../../service/authentification.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  constructor(private authservice: AuthentificationService) { }

  ngOnInit() {
  }
 employ() {
    return this.authservice.isEmployeur();
 }
admin() {
    return this.authservice.isAdmin();
}
client() {
    return this.authservice.isClient();
}
}
