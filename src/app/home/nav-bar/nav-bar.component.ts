import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from '../../service/authentification.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private authServise: AuthentificationService, private router: Router) { }

  ngOnInit() {
  }
out() {
    // this.authServise.logout();
  localStorage.clear();
  this.router.navigate(['']);
}
}
