import { Component, OnInit } from '@angular/core';
import {Employeur} from '../model/employeur';
import {EmployeurService} from '../service/employeur.service';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-employeur',
  templateUrl: './employeur.component.html',
  styleUrls: ['./employeur.component.css']
})
export class EmployeurComponent implements OnInit {
  listeEmployeur;
  added = false;
  submitted = false;
  id;
  roleList;
  employeur = new Employeur();

  constructor(private employeurservice: EmployeurService) {
    this.all();
  }

  ngOnInit() {
  }

  all() {
    this.employeurservice.getAllEmployeur().subscribe(res => {
      console.log(res);
      this.listeEmployeur = res;
    });

  }

  onDelite(id) {
    this.employeurservice.delete(id).subscribe(res => {
      console.log(id);
      this.all();
    });
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.employeur);
    //const data=this.employeur;
//employeur.getCin(), employeur.getNom(), employeur.getPrenom(), employeur.getVille(), employeur.getTelephone(), employeur.getEmail(), employeur.getConfirmedPassword(), employeur.getUsername(), employeur.getPassword(), employeur.getPost(), employeur.getSalaire()
    this.employeurservice.ajouter(this.employeur).subscribe(res => {
      console.log(res);
      this.all();
      this.employeur.cin = 0;
      this.employeur.nom = '';
      this.employeur.prenom = '';
      this.employeur.salaire = 0;
      this.employeur.post = '';
      this.employeur.ville = '';
      this.employeur.telephone = 0;
      this.employeur.email = '';
      this.employeur.username = '';
      this.employeur.password = '';
      this.employeur.confirmedPassword = '';
    });
  }

  recuper(id, cin, nom, prenom, ville, telephone, email, post, salaire, username) {
    this.id = id;
    this.employeur.cin = cin;
    this.employeur.nom = nom;
    this.employeur.prenom = prenom;
    this.employeur.post = post;
    this.employeur.ville = ville;
    this.employeur.telephone = telephone;
    this.employeur.email = email;
    this.employeur.username = username;
    this.employeur.salaire = salaire;

  }

  onUpdate() {
    console.log(this.employeur);
    const data = {
      cin: this.employeur.cin,
      nom: this.employeur.nom,
      prenom: this.employeur.prenom,
      ville: this.employeur.ville,
      telephone: this.employeur.telephone,
      email: this.employeur.email,

      username: this.employeur.username,

      post: this.employeur.post,
      salaire: this.employeur.salaire
    };
    this.employeurservice.update(this.id, data).subscribe(res => {
      console.log(res);
      this.all();
    });

  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({mustMatch: true});
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  closemodal() {
    this.employeur.username = '';
    this.employeur.password = '';
    this.employeur.salaire = 0;
    this.employeur.post = '';
    this.employeur.cin = 0;
    this.employeur.email = '';
    this.employeur.telephone = 0;
    this.employeur.ville = '';
    this.employeur.prenom = '';
    this.employeur.nom = '';
  }
}
