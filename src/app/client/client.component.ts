import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClientService} from '../service/client.service';
import {Client} from '../model/client';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  registerForm: FormGroup;
  tout;
  client: Client;
  submitted = false;
  constructor(private clientservice: ClientService, private formBuilder: FormBuilder) {
    this.all();
  }

  ngOnInit() {
  }

  get f() {
    return this.registerForm.controls;
  }

  all() {
    this.clientservice.getAllClient().subscribe(r => {
      console.log(r);
      this.tout = r;
    });
  }
  onUpdate(id, client) {
this.clientservice.upadate(id, client).subscribe(res => {
  console.log(res);
});
  }
  onDelite(id) {
    this.clientservice.delete(id).subscribe(sta => {
      console.log(sta);
      this.all();
    });
  }
}
