import {Categorie} from './categorie';

export class SousCategorie {
  id: string;
  name: string;
  categorie = new Categorie();
}
