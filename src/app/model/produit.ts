import {Categorie} from './categorie';
import {SousCategorie} from './sous-categorie';

export class Produit {
  id: string;
  sousCategorie = new SousCategorie();
  nom: string;
  status: string;
  time: string;
  photo: string;
  price: number;
  quantite: number;

  // set nom(value: string) {
  //   this._nom = value;
  // }
  //
  // get nom(): string {
  //   return this._nom;
  // }
  //
  // get status(): string {
  //   return this._status;
  // }
  //
  // set status(value: string) {
  //   this._status = value;
  // }
  //
  // get time(): string {
  //   return this._time;
  // }
  //
  // set time(value: string) {
  //   this._time = value;
  // }
  //
  // get photo(): string {
  //   return this._photo;
  // }
  //
  // set photo(value: string) {
  //   this._photo = value;
  // }
  //
  // get price(): number {
  //   return this._price;
  // }
  //
  // set price(value: number) {
  //   this._price = value;
  // }
  //
  // get quantite(): number {
  //   return this._quantite;
  // }
  //
  // set quantite(value: number) {
  //   this._quantite = value;
  // }
}
