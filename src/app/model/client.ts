export class Client {
  private _nom: string;
  private _prenom: string;
  private _cin: number;
  private _adresse: string;
  private _ville: string;
  private _telephone: number;
  private _email: string;
  private _username: string;

  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get prenom(): string {
    return this._prenom;
  }

  set prenom(value: string) {
    this._prenom = value;
  }

  get cin(): number {
    return this._cin;
  }

  set cin(value: number) {
    this._cin = value;
  }

  get adresse(): string {
    return this._adresse;
  }

  set adresse(value: string) {
    this._adresse = value;
  }

  get ville(): string {
    return this._ville;
  }

  set ville(value: string) {
    this._ville = value;
  }

  get telephone(): number {
    return this._telephone;
  }

  set telephone(value: number) {
    this._telephone = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }
}
