import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SideBarComponent } from './home/side-bar/side-bar.component';
import { NavBarComponent } from './home/nav-bar/nav-bar.component';
import { ContainerComponent } from './home/container/container.component';
import { FooterComponent } from './home/footer/footer.component';
import { LoginComponent } from './login/login.component';
import { ProduitsComponent } from './produits/produits.component';
import { FactureComponent } from './facture/facture.component';
import { RegisterComponent } from './register/register.component';
import { ErreurComponent } from './erreur/erreur.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AddproduitComponent } from './addproduit/addproduit.component';
import { ClientComponent } from './client/client.component';
import { AddfactureComponent } from './addfacture/addfacture.component';
import { ModifClientComponent } from './modif-client/modif-client.component';
import { EmployeurComponent } from './employeur/employeur.component';
import { AddemplComponent } from './addempl/addempl.component';
import { CategorieComponent } from './categorie/categorie.component';
import { SousCategorieComponent } from './sous-categorie/sous-categorie.component';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SideBarComponent,
    NavBarComponent,
    ContainerComponent,
    FooterComponent,
    LoginComponent,
    ProduitsComponent,
    FactureComponent,
    RegisterComponent,
    ErreurComponent,
    AddproduitComponent,
    ClientComponent,
    AddfactureComponent,
    ModifClientComponent,
    EmployeurComponent,
    AddemplComponent,
    CategorieComponent,
    SousCategorieComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
