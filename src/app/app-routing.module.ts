import {Component, NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {ContainerComponent} from './home/container/container.component';
import {NavBarComponent} from './home/nav-bar/nav-bar.component';
import {SideBarComponent} from './home/side-bar/side-bar.component';
import {FooterComponent} from './home/footer/footer.component';
import {ProduitsComponent} from './produits/produits.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ErreurComponent} from './erreur/erreur.component';
import {AddproduitComponent} from './addproduit/addproduit.component';
import {FactureComponent} from './facture/facture.component';
import {ClientComponent} from './client/client.component';
import {AuthGuard} from './authgard/auth.guard';
import {AddfactureComponent} from './addfacture/addfacture.component';
import {EmployeurComponent} from './employeur/employeur.component';
import {CategorieComponent} from './categorie/categorie.component';
import {SousCategorieComponent} from './sous-categorie/sous-categorie.component';

const routes: Routes = [
  {path: '', component: LoginComponent, },


  {path: 'home', component: HomeComponent, canActivate: [AuthGuard],
  children: [{path: '', component: ContainerComponent},
    {path: 'produit', component: ProduitsComponent},
    {path: 'employeur', component: EmployeurComponent},
    {path: 'client', component: ClientComponent},
    {path: 'add', component: AddproduitComponent},
    {path: 'addFacture', component: AddfactureComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'categorie', component: CategorieComponent},
    {path: 'sousCategorie', component: SousCategorieComponent},
    {path: 'facture', component: FactureComponent}
  ]
},
  {path: '**', component: ErreurComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
