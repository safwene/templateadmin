import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthentificationService} from '../service/authentification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private authService: AuthentificationService) {
  }
   canActivate() {
    if (this.authService.isAuthenticated()) {
     return true;
   } else {
     // this.authService.parseJWT();
    this.router.navigate(['/']);
    return false;
   }
   }
}
