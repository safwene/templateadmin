import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from '../service/authentification.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor( private authService: AuthentificationService, private  router: Router) { }

  ngOnInit() {
  }
onlogin(data) {
 // localStorage.setItem('token', '1');
  return this.authService.login(data).subscribe(res => {
      const jwt = res.headers.get('Authorization');
      this.authService.saveToken(jwt);

      console.log(res);
      this.router.navigateByUrl('/home');

    }, error2 => {
alert('valider');

});
}
}
