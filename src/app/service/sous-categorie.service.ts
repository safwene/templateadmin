import { Injectable } from '@angular/core';
import {AuthentificationService} from './authentification.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SousCategorieService {


  constructor(private http: HttpClient, private authservice: AuthentificationService) { }
  getAllSousCategories() {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.get('http://localhost:9000/sousCategorie/all', {headers: header});
  }
  ajouter(data) {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.post('http://localhost:9000/sousCategorie/register', data , {headers: header});
  }
  delete(id) {
    const header = new HttpHeaders({authorization: 'Bearer' + this.authservice.jwt});
    return this.http.delete('http://localhost:9000/sousCategorie/delete/' + id, {headers: header});
  }
  upadate(id, data) {
    const header = new HttpHeaders({authorization: 'Bearer' + this.authservice.jwt});
    return this.http.put('http://localhost:9000/sousCategorie/update/' + id, data, {headers: header});
  }

}
