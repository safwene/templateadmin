import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthentificationService} from './authentification.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient, private authservice: AuthentificationService) { }
  getAllClient() {
    const header = new HttpHeaders({authorization: 'Bearer' + this.authservice.jwt});
    return this.http.get('http://localhost:9000/client/all', {headers: header});
  }
//   ajouter(nom, prenom, cin, adresse, ville, telephone, email, username, password, confirmedPassword) {
//     return this.http.post('http://localhost:9000/client/register', {nom:nom , prenom:prenom, cin:cin, adresse:adresse, ville:ville,
//       telephone:telephone, email:email, username:username, password:password, confirmedPassword:confirmedPassword});
// }
  ajouter(data) {
    const header = new HttpHeaders({authorization: 'Bearer' + this.authservice.jwt});
    return this.http.post('http://localhost:9000/client/register', data , {headers: header});
  }
  delete(id) {
    const header = new HttpHeaders({authorization: 'Bearer' + this.authservice.jwt});
    return this.http.delete('http://localhost:9000/client/delete/' + id, {headers: header});
  }

  upadate(id, data) {
    const header = new HttpHeaders({authorization: 'Bearer' + this.authservice.jwt});
    return this.http.put('http://localhost:9000/produit/update/' + id, data, {headers: header});
  }
}
