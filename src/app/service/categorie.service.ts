import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthentificationService} from './authentification.service';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  constructor(private http: HttpClient, private authservice: AuthentificationService) { }
  getAllCategories() {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.get('http://localhost:9000/categories/all', {headers: header});
  }
  ajouter(data) {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.post('http://localhost:9000/categories/register', data , {headers: header});
  }
  delete(id) {
    const header = new HttpHeaders({authorization: 'Bearer' + this.authservice.jwt});
    return this.http.delete('http://localhost:9000/categories/delete/' + id, {headers: header});
  }
  upadate(id, data) {
    const header = new HttpHeaders({authorization: 'Bearer' + this.authservice.jwt});
    return this.http.put('http://localhost:9000/categories/update/' + id, data, {headers: header});
  }
}
