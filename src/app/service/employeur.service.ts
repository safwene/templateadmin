import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthentificationService} from './authentification.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeurService {

  constructor(private http: HttpClient, private authservice: AuthentificationService) { }

  getAllEmployeur() {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.get('http://localhost:9000/employeur/all', {headers: header});
  }

  ajouter(data) {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.post('http://localhost:9000/employeur/register', data , {headers: header});
  }
  delete(id) {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.delete('http://localhost:9000/employeur/delete/' + id , {headers: header});
}
  update(id, data) {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.put('http://localhost:9000/employeur/update/' + id, data, {headers: header});
  }
}
