import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
// baseurl: 'http://localhost:9000';
  jwt: string;
username: string;
  roles: Array<string>;
  constructor( private  http: HttpClient) { }
  login(data) {
    console.log(data);

    return this.http.post('http://localhost:9000' + '/login', data, {observe: 'response'});
  }

  parseJWT() {
    const  jwtHelper = new JwtHelperService();
    const objJWT = jwtHelper.decodeToken(this.jwt);
    this.username = objJWT.obj;
    this.roles = objJWT.roles;
  }
  saveToken(jwt: string) {
    localStorage.setItem('token', jwt);
    this.jwt = jwt;
    this.parseJWT();
  }
  isAdmin() {
    return this.roles.indexOf('ADMIN') >= 0 ;
  }
  isClient() {
    return this.roles.indexOf('CLIENT') >= 0 ;

  }
  isEmployeur() {
    return this.roles.indexOf('EMPLOYEUR') >= 0 ;

  }
  isAuthenticated() {

    return this.roles && (this.isAdmin() || this.isClient() || this.isEmployeur());
  }

  loadToken() {
    this.jwt = localStorage.getItem('token');
    this.parseJWT();
  }

  logout() {
    localStorage.removeItem('token');
    this.initParams();
  }

  initParams() {
    this.jwt = undefined;
    this.username = undefined;
    this.roles = undefined;
  }


}
