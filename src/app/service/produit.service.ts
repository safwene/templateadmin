import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {AuthentificationService} from './authentification.service';


@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor(private http: HttpClient, private router: Router, private authservice: AuthentificationService) {
  }

  getAllProduit() {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.get('http://localhost:9000/produit/all',{headers: header});
  }

  delete(id) {
    const header = new HttpHeaders({authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.delete('http://localhost:9000/produit/delete/' + id,{headers: header});
  }

  upadate(id, produit) {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.put('http://localhost:9000/produit/update/' + id, produit, {headers: header});
  }

  ajouter(id, produit) {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.post('http://localhost:9000/produit/register/' + id , produit, { headers: header});
  }
}
