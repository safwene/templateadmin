import { Component, OnInit } from '@angular/core';
import {ProduitService} from '../service/produit.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Produit} from '../model/produit';
import {ImageService} from '../service/image.service';
import {HttpEventType, HttpResponse} from '@angular/common/http';
import {Categorie} from '../model/categorie';

@Component({
  selector: 'app-addproduit',
  templateUrl: './addproduit.component.html',
  styleUrls: ['./addproduit.component.css']
})
export class AddproduitComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
produit: Produit;
  filesToUpload: Array<File>;
  photo;
  categories = new Categorie();
  constructor(private serviceAddProuit: ProduitService, private formBuilder: FormBuilder, private router: Router, private imageservice: ImageService) {

    this.photo = 'choisir une image' ; }


  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nomCategorie: ['', Validators.required],
      nom: ['', Validators.required],

      quantite: ['', Validators.required],
      price: ['', Validators.required],
      status: ['', Validators.required],
      time: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }
  all() {
    this.serviceAddProuit.getAllProduit().subscribe(res  => {
      // this.authService.loadToken();
      console.log(res);

    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.categories.name = this.registerForm.value.nomCategorie;
    const data = {
        nom: this.registerForm.value.nom,
        photo: this.photo,
        quantite: this.registerForm.value.quantite,
        price: this.registerForm.value.price,
      status: this.registerForm.value.status,
        time: this.registerForm.value.time
      };
     console.log(data);
    this.serviceAddProuit.ajouter(this.categories.name, data).subscribe(r => {
   console.log(r);
  this.imageservice.pushFileToStorage(this.filesToUpload[0]).subscribe(rest => {
        console.log(rest);
      });
  this.registerForm.value.nom = '';
  this.registerForm.value.nomCategorie = '';
  this.photo = 'choisir une image';
  this.registerForm.value.quantite = 0;
  this.registerForm.value.price = 0;
  this.registerForm.value.status = '';
  this.registerForm.value.time = '';
});
    this.router.navigateByUrl('home/produit');
  }

  recuperFile(file) {
    this.filesToUpload = file.target.files as Array<File>;

    this.photo = file.target.files[0].name;
  }
}
