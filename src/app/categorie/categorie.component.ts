import { Component, OnInit } from '@angular/core';
import {Client} from '../model/client';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategorieService} from '../service/categorie.service';
import {Categorie} from '../model/categorie';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.css']
})
export class CategorieComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  categorie = new Categorie();
  tout;

  config: any;
  collection = { count: 60, data: [] };
  constructor(private categorieService: CategorieService) {
    this.all();
  //   //Create dummy data
  //   for (var i = 0; i < this.collection.count; i++) {
  //     this.collection.data.push(
  //       {
  //         id: i + 1,
  //         value: "items number " + (i + 1)
  //       }
  //     );
  //   }
  //
  //   this.config = {
  //     itemsPerPage: 5,
  //     currentPage: 1,
  //     totalItems: this.collection.count
  //   };
  //
  //
  // }
  //
  // pageChanged(event) {
  //   this.config.currentPage = event;
   }



  ngOnInit() {

  }
  onSubmit() {
    this.submitted = true;
    const data = {
      name: this.categorie.name
    };
    this.categorieService.ajouter(data).subscribe(r => {
      console.log(r);
    });
    this.all();
  }



  all() {
    this.categorieService.getAllCategories().subscribe(r => {
      console.log(r);
      this.tout = r;
    });
  }

  onDelite(id) {
    this.categorieService.delete(id).subscribe(sta => {
      console.log(sta);
      this.all();
    });
  }

  onUpdate(id, categorie) {
    this.categorieService.upadate(id, categorie).subscribe(res => {
      console.log(res);
    });

  }
  closemodal() {
    this.categorie.name = '';
    // this.employeur.password= '';
  }


  // recuper(id, cin, nom, prenom, ville, telephone, email, username, password, confirmpassword, post, salaire) {
  //   this.id = id;
  //   this.employeur.cin = cin;
  //
  // }
}
