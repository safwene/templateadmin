import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClientService} from '../service/client.service';
import {Client} from '../model/client';
import {Router} from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
//  client: Client;
  registerForm: FormGroup;
  submitted = false;
  constructor(private clientservice: ClientService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nom: ['', [Validators.required]],
      prenom: ['', Validators.required],
      cin: ['', Validators.required],
      adresse: ['', Validators.required],
      ville: ['', Validators.required],
      telephone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      username: ['', Validators.required],
      password: ['', Validators.required, Validators.minLength(6)],
      confirmedPassword: ['', Validators.required]
    }, {
      validator: this.MustMatch('password', 'confirmedPassword')
    });
  }


  get f() {
    return this.registerForm.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
// console.log(this.registerForm.value);

   const data = {
     nom: this.registerForm.value["nom"],
      prenom: this.registerForm.value["prenom"],
      cin:this.registerForm.value["cin"],
      adresse:this.registerForm.value["adresse"],
        ville:this.registerForm.value["ville"],
      telephone:this.registerForm.value["telephone"],
     email: this.registerForm.value["email"],
      username: this.registerForm.value["username"],
      password: this.registerForm.value["password"],

      confirmedPassword: this.registerForm.value["confirmedPassword"]

    }

    console.log(data);
      //result= this.registerForm.value;

  this.clientservice.ajouter(data).subscribe(res=>{

       console.log(res);
    this.router.navigateByUrl('/home');
    });
   /* this.clientservice.ajouter(this.registerForm.value["nom"], this.registerForm.value["prenom"], this.registerForm.value["cin"], this.registerForm.value["adresse"], this.registerForm.value["ville"], this.registerForm.value["telephone"], this.registerForm.value["email"], this.registerForm.value["username"], this.registerForm.value["password"], this.registerForm.value["confirmedPassword"])
      .subscribe(res=> {
      console.log(res);
    });*/
    // this.router.navigateByUrl('/home');
  }


  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }
}
