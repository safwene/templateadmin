import { Component, OnInit } from '@angular/core';
import {CategorieService} from '../service/categorie.service';
import {Categorie} from '../model/categorie';
import {FormGroup} from '@angular/forms';
import {SousCategorie} from '../model/sous-categorie';
import {SousCategorieService} from '../service/sous-categorie.service';

@Component({
  selector: 'app-sous-categorie',
  templateUrl: './sous-categorie.component.html',
  styleUrls: ['./sous-categorie.component.css']
})
export class SousCategorieComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  sousCategorie = new SousCategorie();
  tout;


  constructor(private categorieService: SousCategorieService) {
    this.all();
  }

  ngOnInit() {

  }
  onSubmit() {
    this.submitted = true;
    const data = {
      name: this.sousCategorie.name
    };
    this.categorieService.ajouter(data).subscribe(r => {
      console.log(r);
    });
    this.all();
  }



  all() {
    this.categorieService.getAllSousCategories().subscribe(r => {
      console.log(r);
      this.tout = r;
    });
  }

  onDelite(id) {
    this.categorieService.delete(id).subscribe(sta => {
      console.log(sta);
      this.all();
    });
  }

  onUpdate(id, categorie) {
    this.categorieService.upadate(id, categorie).subscribe(res => {
      console.log(res);
    });

  }
  closemodal() {
    this.sousCategorie.name = '';
    // this.employeur.password= '';
  }
}
