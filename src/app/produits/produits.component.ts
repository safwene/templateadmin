import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from '../service/authentification.service';
import {Router} from '@angular/router';
import {ProduitService} from '../service/produit.service';
import {Produit} from '../model/produit';
import {HttpResponse} from '@angular/common/http';
import {ImageService} from '../service/image.service';
import {FormGroup} from '@angular/forms';
import {Categorie} from '../model/categorie';
import Swal from 'sweetalert2';
import {SousCategorie} from '../model/sous-categorie';

@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.css']
})
export class ProduitsComponent implements OnInit {
  produit = new Produit();
   resulta;
   submitted = false;
   photo;
  filesToUpload: Array<File>;
  sousCategorie = new SousCategorie();
  id;
  idc;
  catego;
  constructor(private produitService: ProduitService, private  router: Router, private imageservice: ImageService) {
    this.all();
    this.photo = 'choisir une image' ;
  }

  ngOnInit() {
  }
all() {
    this.produitService.getAllProduit().subscribe(res  => {
     // this.authService.loadToken();
      console.log(res);
      this.resulta = res;
      // this.catego = res.get;

    });
}
onDelite(id) {
  Swal.fire({
    title: 'Are you sure?',
    text: 'You won\'t be able to revert this!',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      this.produitService.delete(id).subscribe(res => {
        console.log(id);
        this.all();
      });
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      );
    }
  });


}
  recuper(id, SouscategorieName, photo, nom, quantite, price, time, status, IdC) {
    this.id = id;
    this.idc = IdC;
    this.produit.sousCategorie.name = SouscategorieName;
    this.produit.nom = nom;
    this.photo = photo;
    this.produit.quantite = quantite;
    this.produit.price = price;
    this.produit.time = time;
    this.produit.status = status;
    // this.categorie.id = categorieId;
}
  recuperFile(file) {
    this.filesToUpload = file.target.files as Array<File>;

    this.photo = file.target.files[0].name;
  }
onUpdate() {

    this.submitted = true;
    this.produit.sousCategorie.id = this.idc;
    this.produit.sousCategorie.name = this.produit.sousCategorie.name;
    const data = {
      sousCategorie: this.produit.sousCategorie,
    nom: this.produit.nom,
    quantite: this.produit.quantite,
    price: this.produit.price,
    time: this.produit.time,
    photo: this.photo,
    status: this.produit.status
  };
   // console.log(data);
    return this.produitService.upadate(this.id, data).subscribe(r => {
      console.log(r);
      if (this.filesToUpload !== undefined) {

      this.imageservice.pushFileToStorage(this.filesToUpload[0]).subscribe(rest => {
        console.log(rest);



      });
    }
      Swal.fire({
        position: 'top-end',
        type: 'success',
        title: 'Modifie avec succes',
        showConfirmButton: false,
        timer: 1500
      });

      this.produit = new Produit();
      this.all();
    });
  this.closemodal();
}
  closemodal() {
    this.produit.sousCategorie.name = '';
    this.produit.nom = '';
    this.produit.photo = 'choisir une image';
    this.produit.time = '';
    this.produit.status = '';
    this.produit.price = 0;
    this.produit.quantite = 0;
  }
  // MustMatch(controlName: string, matchingControlName: string) {
  //   return (formGroup: FormGroup) => {
  //     const control = formGroup.controls[controlName];
  //     const matchingControl = formGroup.controls[matchingControlName];
  //
  //     if (matchingControl.errors && !matchingControl.errors.mustMatch) {
  //       // return if another validator has already found an error on the matchingControl
  //       return;
  //     }
  //
  //     // set error on matchingControl if validation fails
  //     if (control.value !== matchingControl.value) {
  //       matchingControl.setErrors({ mustMatch: true });
  //     } else {
  //       matchingControl.setErrors(null);
  //     }
  //   };
  // }

}
